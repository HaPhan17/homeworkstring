import java.util.regex.Pattern;

public class CutString {

	public static String cutStringFunction(String fname, String lname, String link, String s) {
		final String REGEX_START = Pattern.quote("{{");
		final String REGEX_END = Pattern.quote("}}");
		String[] array = s.split(REGEX_START + "(.*?)" + REGEX_END);

		String string = array[0].toString() + fname + array[1].toString() + "<a href=\"" + link + "\""
				+ "/>"+ array[2].toString() + lname;
		return string;
	}

	public static void main(String[] args) {

		String string = "Hello This is first name {{fname}} </br> This is link {{link|click here}} This is last name {{lname}}";
		String fname = "Ha";
		String lname = "Phan";
		String link = "www.google.com";

		String s = cutStringFunction(fname, lname, link, string);
		System.out.println(s);

	}

}
